package services;

import java.util.Random;

import metiers.*;

public class Game {
    private Board board;
    private Asterix asterix;
    private Obelix obelix;
    private Panoramix panoramix;
    private Abraracourcix abraracourcix;
    private boolean released;

    public Game(int size){
        this.board = new Board(size);
        Cell cellAsterix = new Cell(new Random().nextInt(size), new Random().nextInt(size), "Normal");
        this.asterix = new Asterix(cellAsterix, 10, 10);
        Cell cellObelix = new Cell(new Random().nextInt(size), new Random().nextInt(size), "Normal");
        this.obelix = new Obelix(cellObelix, 15, 15);
        Cell cellPanoramix = new Cell(new Random().nextInt(size), new Random().nextInt(size), "Normal");
        this.panoramix = new Panoramix(cellPanoramix, 10, 10);
        Cell cellAbraracourcix = new Cell(new Random().nextInt(size), new Random().nextInt(size), "Normal");
        this.asterix = new Asterix(cellAbraracourcix, 10, 10);
        this.released = false;
    }


    public Board getBoard() {
        return this.board;
    }

    public void setBoard(Board board) {
        this.board = board;
    }

    public Asterix getAsterix() {
        return this.asterix;
    }

    public void setAsterix(Asterix asterix) {
        this.asterix = asterix;
    }

    public Obelix getObelix() {
        return this.obelix;
    }

    public void setObelix(Obelix obelix) {
        this.obelix = obelix;
    }

    public Panoramix getPanoramix() {
        return this.panoramix;
    }

    public void setPanoramix(Panoramix panoramix) {
        this.panoramix = panoramix;
    }

    public Abraracourcix getAbraracourcix() {
        return this.abraracourcix;
    }

    public void setAbraracourcix(Abraracourcix abraracourcix) {
        this.abraracourcix = abraracourcix;
    }


    public void initialize(){
        for (int i = 0; i < this.board.getSize(); ++i) {
            for (int j = 0; j < this.board.getSize(); ++j) {
                this.board.createRandomCell(i, j);
            }
        }

        Cell [][] newGrid = this.board.getGrid();
        int xFalbala = new Random().nextInt(this.board.getSize());
        int yFalbala = new Random().nextInt(this.board.getSize());
        Cell falbala = new Cell(xFalbala, yFalbala, "Falbala");
        newGrid[xFalbala][yFalbala] = falbala;
        this.board.setFalbala(falbala);
        this.board.setGrid(newGrid);
    }

    public void turn(){
        if (this.panoramix.awaken()){
            panoramix.withFalbala(board);
            panoramix.move(this.board);
            Cell cellPanoramix = panoramix.getCoord();
            if(cellPanoramix.getType()=="Beer"){
                Beer beer = (Beer) cellPanoramix;
                panoramix.gainHp(beer.getHp());
            }
            if (cellPanoramix.getType()=="Soldiers"){
                Soldier soldier = (Soldier) cellPanoramix;
                panoramix.loseHp(soldier.getNumber());
            }
        }

        if (this.asterix.awaken()){
            if(asterix.withFalbala(board)){this.released = true;}
            asterix.move(this.board);
            Cell cellAsterix = asterix.getCoord();
            if(cellAsterix.getType()=="Beer"){
                Beer beer = (Beer) cellAsterix;
                asterix.gainHp(beer.getHp());
            }
            if(cellAsterix.getType()=="Soldiers"){
                Soldier soldier = (Soldier) cellAsterix;
                asterix.loseHp(soldier.getNumber());
            }
        }

        if (this.obelix.awaken()){
            if(obelix.withFalbala(board)){this.released = true;}
            obelix.move(this.board);
            Cell cellObelix = obelix.getCoord();
            if(cellObelix.getType()=="Beer"){
                Beer beer = (Beer) cellObelix;
                obelix.gainHp(beer.getHp());
            }
            if(cellObelix.getType()=="Soldiers"){
                Soldier soldier = (Soldier) cellObelix;
                obelix.loseHp(soldier.getNumber());
            }
        }

        if (this.abraracourcix.awaken()){
            if(abraracourcix.withFalbala(board)){this.released = true;}
            abraracourcix.move(this.board);
            Cell cellAbraracourcix = asterix.getCoord();
            if(cellAbraracourcix.getType()=="Beer"){
                Beer beer = (Beer) cellAbraracourcix;
                abraracourcix.gainHp(beer.getHp());
            }
            if(cellAbraracourcix.getType()=="Soldiers"){
                Soldier soldier = (Soldier) cellAbraracourcix;
                abraracourcix.loseHp(soldier.getNumber());
                abraracourcix.useShield();
            }
        }

        asterix.isLowHp();
        asterix.withPanoramix(panoramix);
        panoramix.heal(asterix);
        panoramix.heal(obelix);
        obelix.isHungry();

    } 


    public void play (){
        int numberturn = 0;
        while ((numberturn<150)&&(!(this.released))){
            this.turn();
            numberturn += 1;
        }
    }
}
