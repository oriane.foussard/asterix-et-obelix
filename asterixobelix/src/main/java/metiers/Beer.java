package metiers;

/**
 * Classe héritée de cell qui possède une bière.
 *
 * Cette classe est un bonus qui permet à un personnage de recharger ses points
 * de vie
 * lorsqu'il passe dessus pour la première fois.
 *
 * @author Oriane Foussard
 * @version 1.0
 * @since 7/06/22
 */

public class Beer extends Cell {
    private int hp;

<<<<<<< HEAD
    /**
     * Initialisation de la classe bière
     *
     * @param x    La coordonée x de la case
     * 
     * @param y    La coordonée y de la case
     * 
     * @param type le type de la case (bière ici)
     * 
     * @param hp   le nombre de points de vie que le personnage récupère sur la case
     * 
     * @since 1.0
     */
    public Beer(int x, int y, String type, int hp) {
        super(x, y, type);
=======
    public Beer(int x, int y, String type,int hp){
        super(x, y, "Beer");
>>>>>>> 7996c7a639602d14bc83a4c9a363fdb5a348a9b4
        this.hp = hp;
    }

    public int getHp() {
        return this.hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

}
