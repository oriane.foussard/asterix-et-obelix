package metiers;

public class Abraracourcix extends Character{

    public Abraracourcix(Cell coord, int hp, int maxHp) {
        super("Abraracourcix", coord, hp, maxHp);
        //TODO Auto-generated constructor stub
    }

    public void blockDamage (){
        if (this.getCoord().getType() == "Soldiers" ){
            Soldier soldier = (Soldier) this.getCoord();
            this.setHp( this.getHp() + soldier.getNumber());
        }
    }

    public void useShield(){
        double proba = Math.random();
        if (proba < 0.4){
            this.blockDamage();
        }
    }
    
}
