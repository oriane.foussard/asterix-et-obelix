package metiers;

public class Obelix extends Character{

    public Obelix(Cell coord, int hp, int maxHp) {
        super("Obelix", coord, hp, maxHp);
        //TODO Auto-generated constructor stub
    }

    public void eatBoard(){
        this.setHp(this.getMaxHp());
    }

    public void isHungry(){
        double proba = Math.random();
        if (proba < 0.2){
            this.eatBoard();
        }
    }
    
}
