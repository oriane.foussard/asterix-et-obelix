package metiers;

public class Asterix extends Character{
    private boolean potion ;

    public Asterix(Cell coord, int hp, int maxHp) {
        super("Asterix", coord, hp, maxHp);
        //TODO Auto-generated constructor stub
        this.potion = true;
    }


    public boolean isPotion() {
        return this.potion;
    }

    public boolean getPotion() {
        return this.potion;
    }

    public void setPotion(boolean potion) {
        this.potion = potion;
    }

    public void rechargePotion(){
        this.potion = true;
    }

    public void usePotion(){
        if (this.isPotion()){
            this.potion = false;
            this.setHp(this.getMaxHp());
        }
    }

    public void withPanoramix(Panoramix panoramix){
        if ((this.getCoord().getX() == panoramix.getCoord().getX())&&(this.getCoord().getY() == panoramix.getCoord().getY())){
            this.rechargePotion();
        }
    }

    public void isLowHp(){
        if (this.getHp() < 3){
            this.usePotion();
        }
    }

}
