package metiers;

public enum Direction {
    LEFT("left", -1, 0),
    RIGHT("right", 1, 0),
    UP("up", 0, 1),
    DOWN("down", 0, -1);


    private final String name ;
    private final int x ;
    private final int y;

    private Direction(String name, int x, int y){
        this.name = name;
        this.x = x;
        this.y = y;

    }

    public String getName() {
        return this.name;
    }


    public int getX() {
        return this.x;
    }


    public int getY() {
        return this.y;
    }



}
