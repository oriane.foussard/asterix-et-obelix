package metiers;

public class Panoramix extends Character{

    public Panoramix(Cell coord, int hp, int maxHp) {
        super("Panoramix", coord, hp, maxHp);
        //TODO Auto-generated constructor stub
    }

    public void heal(Character personnage){
        personnage.setHp(personnage.getMaxHp());
    }

    public void isGrouped(Character personnage){
        int xPersonnage = personnage.getCoord().getX();
        int yPersonnage = personnage.getCoord().getY();
        int xPanoramix = this.getCoord().getX();
        int yPanoramix = this.getCoord().getY();
        if ((xPersonnage==xPanoramix)&&(yPersonnage==yPanoramix)){
            if (!(personnage.getName()=="Obelix")) {
                heal(personnage);
            }
        }
    }
}
