package metiers;

public class Soldier extends Cell{
    private int number;

    public Soldier(int x, int y, String type,int number){
        super(x, y, "Soldiers");
        this.number = number;
    }


    public int getNumber() {
        return this.number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

}
