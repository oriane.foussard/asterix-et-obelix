package metiers;

<<<<<<< HEAD
/**
 * Classe abstraite qui permet de représenter les personnages.
 *
 * Il n'existe pas de personnages instanciés directement avec cette classe
 * actuellement.
 * Cependant, il serait possible de le faire pour un personnage neutre.
 * Cette classe contient les principaux attributs d'un personnage, comme ces
 * points de vie par exemple.
 *
 * @author Oriane Foussard
 * @version 1.0
 * @since 7/06/22
 */
=======
import java.util.Random;
>>>>>>> 7996c7a639602d14bc83a4c9a363fdb5a348a9b4

public class Character {
    private final String name;
    private Cell coord;
    private int hp;
    private final int maxHp;

    /**
     * Initialisation de la classe abstraite personnage.
     *
     * @param name  Le nom du personnage
     *
     * @param coord Les coordonées du personnage
     * 
     * @param hp    Les points de vie du personnage
     * 
     * @param maxHP Les points de vie maximum du personnage
     * 
     * @since 1.0
     */
    public Character(String name, Cell coord, int hp, int maxHp) {
        this.name = name;
        this.coord = coord;
        this.hp = hp;
        this.maxHp = maxHp;
    }

    public String getName() {
        return this.name;
    }

    public Cell getCoord() {
        return this.coord;
    }

    public void setCoord(Cell coord) {
        this.coord = coord;
    }

    public int getHp() {
        return this.hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getMaxHp() {
        return this.maxHp;
    }

    /**
     * Retirer des points de vie du personnage
     *
     * @param pv les points de vie à retirer au personnage
     * 
     * @since 1.0
     */

    public void loseHp(int pv) {
        int potentialHp = getHp() - pv;
        if (potentialHp < 0) {
            setHp(0);
        } else {
            setHp(potentialHp);
        }
    }

    /**
     * Remettre des points de vie à un personnage
     *
     * @param pv Les points de vie à rajouter au personnage en question
     *
     * @since 1.0
     */
    public void gainHp(int pv) {
        int potentialHp = getHp() + pv;
        if (potentialHp > getMaxHp()) {
            setHp(getMaxHp());
        } else {
            setHp(potentialHp);
        }
    }

    /**
     * Détermination de l'état du personnage.
     *
     * @return Le personnage est-il réveillé (true) ou assomé (false)
     * 
     * @since 1.0
     */
    public boolean awaken() {
        if (getHp() == 0) {
            return false;
        } else
            return true;

    }

    public void move (Board board){
        int iddirection = new Random().nextInt(4);
        Direction [] directions = {Direction.DOWN, Direction.UP, Direction.LEFT, Direction.RIGHT} ;
        Direction direction = directions[iddirection];

        int computer = 0;
        while (!(board.possibleMove(this, direction))&&(computer<4)){
            iddirection = new Random().nextInt(4);
            direction = directions[iddirection];
        }
        if (board.possibleMove(this, direction)){
            Cell actualcell = this.getCoord();
            int x = actualcell.getX() + direction.getX();
            int y = actualcell.getY() + direction.getY();
            Cell cell = board.getGrid()[x][y];
            this.setCoord(cell);
        }
        if (computer==4){
            this.loseHp(1);
            Cell actualcell = this.getCoord();
                int x = actualcell.getX() + direction.getX();
                int y = actualcell.getY() + direction.getY();
                Cell cell = new Cell(x, y, "Normal");
                this.setCoord(cell);
                Cell[][] grid = board.getGrid();
                grid[x][y] = cell;
                board.setGrid(grid);
        }

    }

    public boolean withFalbala(Board board){
        Cell falbala = board.getFalbala();
        if ((this.getCoord().getX() == falbala.getX())&&(this.getCoord().getY() == falbala.getY())){
            return true;
        }
        return false;
        
    }

}
