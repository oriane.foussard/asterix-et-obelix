package metiers;

import java.util.Random;

/**
 * Classe abstraite qui permet de représenter le plateau de jeu.
 *
 * Cette classe contient l'ensemble des variables du plateau.
 * On peut l'initialiser et faire avancer la partie en faisant jouer un tour.
 *
 * @author Oriane Foussard
 * @version 1.0
 * @since 7/06/22
 */

public class Board {
    private final int size;
    private final int difficulty;
<<<<<<< HEAD
    private Cell[][] grid;
    private ArrayList<Wall> walls;
=======
    private Cell [][] grid;
>>>>>>> 7996c7a639602d14bc83a4c9a363fdb5a348a9b4
    private Cell falbala;

    /**
     * Initialisation de la classe Board.
     *
     * @param size La taille de la grille
     * 
     * @since 1.0
     */
    public Board(int size) {
        this.size = size;
        this.difficulty = 0;
        this.grid = new Cell [size] [size];
        int xFalbala = new Random().nextInt(size);
        int yFalbala = new Random().nextInt(size);
        this.grid[xFalbala][yFalbala] = new Cell(xFalbala, yFalbala, "Falbala");
        this.falbala = this.grid[xFalbala][yFalbala];
    }

    public int getSize() {
        return this.size;
    }

    public int getDifficulty() {
        return this.difficulty;
    }

    public Cell[][] getGrid() {
        return this.grid;
    }

    public void setGrid(Cell[][] grid) {
        this.grid = grid;
    }

    public Cell getFalbala() {
        return this.falbala;
    }
    public void setFalbala(Cell falbala) {
        this.falbala = falbala;
    }

    public boolean isInGrid(int potentialX, int potentialY){
        if ((potentialX>=0)&&(potentialX<this.size)&&(potentialY>=0)&&(potentialY<this.size)){
            return true;
        }
        else {return false;}
    }

    public boolean possibleMove(Character personnage, Direction direction) {
        int potentialX = personnage.getCoord().getX() + direction.getX();
        int potentialY = personnage.getCoord().getY() + direction.getY();
        boolean res = true;
<<<<<<< HEAD
        if ((potentialX >= 0) && (potentialX <= this.size) && (potentialY >= 0) && (potentialY <= this.size)) {
            for (Wall wall : this.walls) {
                if (((potentialX == wall.getX1()) && (potentialY == wall.getY1()))
                        || ((potentialX == wall.getX2()) && (potentialX == wall.getY2()))) {
                    res = false;
                }
            }
        } else {
            res = false;
        }
=======
        if (isInGrid(potentialX, potentialY)){
            if(this.grid[potentialX][potentialY].getType() == "Wall"){
                res = false;
            }}
        else {res =false;}
>>>>>>> 7996c7a639602d14bc83a4c9a363fdb5a348a9b4
        return res;
    }

    public void createRandomCell(int indexi, int indexj) {
        double proba = Math.random();
<<<<<<< HEAD
        if (proba < 0.1) {
            int hp = new Random().nextInt(4) + 1; // [0...3] + 1 = [1...4]
            this.grid[indexi][indexj] = new Beer(this.grid[indexi][indexj].getX(), this.grid[indexi][indexj].getY(),
                    "Beer", hp);
        }
        if (proba > 0.6) {
            int number = new Random().nextInt(3) + 1; // [0...2] + 1 = [1...3]
            this.grid[indexi][indexj] = new Soldier(this.grid[indexi][indexj].getX(), this.grid[indexi][indexj].getY(),
                    "Soldiers", number);
        } else {
            this.grid[indexi][indexj] = new Cell(this.grid[indexi][indexj].getX(), this.grid[indexi][indexj].getY(),
                    "Normal");
=======
        if ((proba < 0.1)&&(!(this.grid[indexi][indexj].getType()=="Wall"))){
            int hp = new Random().nextInt(4)  + 1; // [0...3]  + 1 = [1...4]
          this.grid[indexi][indexj] = new Beer(this.grid[indexi][indexj].getX(), this.grid[indexi][indexj].getY(), "Beer", hp);
        }
        if ((proba > 0.6)&&(proba < 0.9)&&(!(this.grid[indexi][indexj].getType()=="Wall"))){
            int number = new Random().nextInt(3)  + 1; // [0...2]  + 1 = [1...3]
          this.grid[indexi][indexj] = new Soldier(this.grid[indexi][indexj].getX(), this.grid[indexi][indexj].getY(), "Soldiers", number);
        }
        if ((proba >= 0.1)&&(proba <= 0.6)&&(!(this.grid[indexi][indexj].getType()=="Wall"))) {
            this.grid[indexi][indexj] = new Cell(this.grid[indexi][indexj].getX(), this.grid[indexi][indexj].getY(), "Normal");
>>>>>>> 7996c7a639602d14bc83a4c9a363fdb5a348a9b4
        }
        if ((proba >= 0.9)&&(!(this.grid[indexi][indexj].getType()=="Wall"))){
            int iddirection = new Random().nextInt(4);
            Direction [] directions = {Direction.DOWN, Direction.UP, Direction.LEFT, Direction.RIGHT} ;
            Direction direction = directions[iddirection];
            int potentialX2 = this.grid[indexi][indexj].getX()+direction.getX();
            int potentialY2 = this.grid[indexi][indexj].getY()+direction.getY();
            boolean notCreate = true;
            while (notCreate){
                if (isInGrid(potentialX2, potentialY2)){
                    this.grid[indexi][indexj] = new Wall(this.grid[indexi][indexj].getX(), this.grid[indexi][indexj].getY(), "Wall");
                    this.grid[potentialX2][potentialY2] = new Wall(this.grid[potentialX2][potentialY2].getX(), this.grid[potentialX2][potentialY2].getY(), "Wall");
                    notCreate = false;
                }
                else {
                    iddirection = new Random().nextInt(4);
                    direction = directions[iddirection];
                    potentialX2 = this.grid[indexi][indexj].getX()+direction.getX();
                    potentialY2 = this.grid[indexi][indexj].getY()+direction.getY();
                }
        }
    }
}
public static void main(String [] args) {
    Cell array [] [] = new Cell [10] [10];
    array[3][3] = new Soldier(3,3,"Wall",4);
    Soldier soldier = (Soldier) array[3][3];
    Board board = new Board(5);
    System.out.println(soldier.getNumber()); 
    System.out.println(board.getSize());}

}
